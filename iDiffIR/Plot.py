# Copyright (C) 2013 by Colorado State University
# Contact: Michael Hamilton <hamiltom@cs.colostate.edu>
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or (at
# your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307,
# USA.
#
import matplotlib
from matplotlib import pyplot as plt
from itertools import chain
import os
import sys
import numpy
from SpliceGrapher.plot.PlotterConfig import SinglePlotConfig, ConfigParser
from SpliceGrapher.plot.PlotUtils import (get_title_padding, generate_plot, plot_legend, plot_splice_graph,
                                          load_gene_data, initialize_plots, get_available_height, AXIS_LEFT, AXIS_WIDTH)
from SpliceGrapher.shared.GeneModelConverter import make_splice_graph, gene_model_to_splice_graph
from SpliceGrapher.view.GeneView import GeneView
from SpliceGrapher.view.SpliceGraphView import SpliceGraphView
from SpliceGrapher.view.ViewerUtils import setXticks, plot_splice_graph
from SpliceGrapher.shared.adjust import get_graph_ranges, adjust_position, adjust_depths

matplotlib.use('agg')

sys.setrecursionlimit(10000)
from iDiffIR.BamfileIO import get_depths_from_bamfiles

# warnings.filterwarnings('ignore')
from matplotlib import rc

rc('text', usetex=True)
import numpy as np

DEFAULT_FONT = 12
DEFAULT_HEIGHT = 11.0
DEFAULT_WIDTH = 8.5


def plotGBCs(f1Codes, f2Codes, f1Cnt, f2Cnt, rmi, odir=os.getcwd()):
    """
    Plot Global Bias Curves (GBCs) for both
    conditions for each replicate.
    """
    L = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M']
    numClusts = len(f1Codes[0])
    f2map = {}  # {0:0,1:1,2:2,3:3,4:4 }
    for i in range(numClusts):
        mC = None
        mV = 0
        for j in range(numClusts):
            cos = (numpy.dot(f1Codes[0][i], f2Codes[0][j]) /
                   (numpy.linalg.norm(f1Codes[0][i]) * numpy.linalg.norm(f2Codes[0][j])))
            if cos > mV:
                mC = j
                mV = cos

        f2map[i] = mC

    plt.cla()
    plt.figure()
    plt.hold(True)
    for i in range(numClusts):
        plt.plot(f1Codes[0][i], '--', marker='.', lw=2, label=L[i])
    plt.legend(ncol=5, loc='upper center')
    plt.xlabel('Bins')
    plt.ylabel('Proportion of reads')
    plt.xticks(range(10), range(1, 11))
    plt.savefig(os.path.join(odir, 'figures', 'f1codes.pdf'))
    plt.hold(False)
    plt.cla()
    plt.figure()
    plt.hold(True)
    for i in range(numClusts):
        plt.plot(f2Codes[0][f2map[i]], '--', marker='.', lw=2, label=L[i])
    plt.legend(ncol=5, loc='upper center')
    plt.xlabel('Bins')
    plt.ylabel('Proportion of reads')
    plt.xticks(range(10), range(1, 11))
    plt.savefig(os.path.join(odir, 'figures', 'f2codes.pdf'))
    plt.hold(False)
    plt.close()
    with open(os.path.join(odir, 'lists', 'curveCounts.txt'), 'w') as fout:
        fout.write('f1: %s\n' % (','.join(['%s:%d' % (L[key], f1Cnt[key]) for key in f1Cnt])))
        fout.write('f2: %s\n' % (','.join(['%s:%d' % (L[f2map[key]], f2Cnt[f2map[key]]) for key in f2Cnt])))
        fout.write("%f\n" % rmi)


def writeAll(geneRecords, aVals, odir=os.getcwd()):
    """
    Write a file of all intron values 
    """
    # iterate across all genes
    with open(os.path.join(odir, 'allIntrons.txt'), 'w') as fout:
        fout.write('\t'.join(['geneID', 'lowExonCoords',
                              'intronCoords', 'highExonCoords',
                              'pValue', 'adjPValue',
                              'logFoldChange', 'intronExp', 'statistic',
                              'bestA', 'known', 'IRAratio_1', 'IRAratio_2', 'IRRratio_1', 'IRRratio_2',
                              'IRR_ratio_diff']) + '\n')
        for gene in geneRecords:
            # iterate across all exons
            if not gene.IRGTested: continue
            for i, intron in enumerate(gene.introns):
                if not gene.IRTested[i]: continue
                fout.write('%s\t%s\t%s\t%s\t%f\t%f\t%f\t%f\t%f\t%d\t%s\t%f\t%f\t%f\t%f\t%f\n' % \
                           (gene.gid, str(gene.exonsR[i]), str(gene.intronsR[i]), str(gene.exonsR[i + 1]),
                            min(1, min(gene.IRPvals[i])), min(1, min(gene.IRQvals[i])), gene.IRfc[i],
                            gene.IRexp[i], gene.IRstat[i][numpy.argmin(gene.IRPvals[i])],
                            aVals[numpy.argmin(gene.IRPvals[i])], str(gene.retained[i]),
                            gene.IRArat1[i],
                            gene.IRArat2[i],
                            gene.IRRrat1[i],
                            gene.IRRrat2[i],
                            gene.IRRrat1[i] - gene.IRRrat2[i]))


def write_all_se(gene_records, a_vals, odir=os.getcwd()):
    """
    Write a file of all intron values 
    """
    # iterate across all genes
    with open(os.path.join(odir, 'allSEs.txt'), 'w') as fout:

        fout.write('\t'.join(['geneID', 'lowExonCoords',
                              'SECoords', 'highExonCoords',
                              'pValue', 'adjPValue',
                              'logFoldChange', 'SEExp', 'statistic',
                              'bestA']) + '\n')
        for gene in gene_records:
            if not gene.SEGTested: continue
            # iterate across all exons
            for i, event in enumerate(gene.flavorDict['SE']):
                if not gene.SETested[i]: continue
                fout.write('%s\t%s\t%s\t%s\t%f\t%f\t%f\t%f\t%f\t%d\n' % \
                           (gene.gid, str(event[0]), str(event[1]),
                            str(event[2]), min(1, min(gene.SEPvals[i])),
                            min(1, min(gene.SEQvals[i])), gene.SEfc[i],
                            gene.SEexp[i],
                            gene.SEstat[i][numpy.argmin(gene.SEPvals[i])],
                            a_vals[numpy.argmin(gene.SEPvals[i])]))


def write_lists(summary_dict, odir=os.getcwd()):
    """
    Write gene and intron lists to file
    """
    # genes all
    with open(os.path.join(odir, 'allDIRGenes.txt'), 'w') as fout:
        all_dir_genes = sorted(set.union(*[summary_dict['known']['upgenes'], summary_dict['known']['downgenes'],
                                         summary_dict['novel']['upgenes'], summary_dict['novel']['downgenes']]))
        for gid in all_dir_genes:
            fout.write('%s\n' % gid)
    # genes up
    with open(os.path.join(odir, 'upDIRGenes.txt'), 'w') as fout:
        up_dir_genes = sorted(set.union(*[summary_dict['known']['upgenes'], summary_dict['novel']['upgenes']]))
        for gid in up_dir_genes:
            fout.write('%s\n' % gid)
    # genes down
    with open(os.path.join(odir, 'downDIRGenes.txt'), 'w') as fout:
        down_dir_genes = sorted(set.union(*[summary_dict['known']['downgenes'], summary_dict['novel']['downgenes']]))
        for gid in down_dir_genes:
            fout.write('%s\n' % gid)

    # IRs all
    with open(os.path.join(odir, 'allDIRs.txt'), 'w') as fout:
        allDIRs = sorted(set.union(*[summary_dict['known']['upirs'], summary_dict['known']['downirs'],
                                     summary_dict['novel']['upirs'], summary_dict['novel']['downirs']]))
        for gid in allDIRs:
            fout.write('%s\n' % gid)
    # IRs up
    with open(os.path.join(odir, 'upDIRs.txt'), 'w') as fout:
        upDIRs = sorted(set.union(*[summary_dict['known']['upirs'], summary_dict['novel']['upirs']]))
        for gid in upDIRs:
            fout.write('%s\n' % gid)
    # IRs down
    with open(os.path.join(odir, 'downDIRs.txt'), 'w') as fout:
        downDIRs = sorted(set.union(*[summary_dict['known']['downirs'], summary_dict['novel']['downirs']]))
        for gid in downDIRs:
            fout.write('%s\n' % gid)


def write_gene_expression(genes, odir):
    # genes expression
    with open(os.path.join(odir, 'geneExp.txt'), 'w') as fout:
        for gene in genes:
            fout.write('%s\t%f\t%f\t%f\n' % (gene.gid, gene.f1exp, gene.f2exp, gene.fc))


def full_tex_table(summary_table, odir=os.getcwd()):
    handle = open(os.path.join(odir, 'dirTable.tex'), 'w')
    ks = sorted(summary_table.keys())[:-2]
    # upregulated
    handle.write('& Up')
    for key in ks:
        if type(key) != int: continue
        handle.write(" & %d/%d" % (len(summary_table[key]['known']['upirs']),
                                   len(summary_table[key]['novel']['upirs'])))
    k = [summary_table[key]['known']['upirs'] for key in ks if type(key) == int]
    n = [summary_table[key]['novel']['upirs'] for key in ks if type(key) == int]
    handle.write('& %d/%d' % (len(set.union(*k)), len(set.union(*n))))
    handle.write('\\\\')
    handle.write('\n')

    # downregulated
    handle.write('& Down')
    for key in ks:
        if type(key) != int: continue
        handle.write(" & %d/%d" % (len(summary_table[key]['known']['downirs']),
                                   len(summary_table[key]['novel']['downirs'])))
    k = [summary_table[key]['known']['downirs'] for key in ks if type(key) == int]
    n = [summary_table[key]['novel']['downirs'] for key in ks if type(key) == int]
    handle.write('& %d/%d' % (len(set.union(*k)), len(set.union(*n))))
    handle.write('\\\\')
    handle.write('\n')

    # both
    handle.write('& Tot')
    for key in ks:
        if type(key) != int: continue
        handle.write(
            " & %d/%d" % (len(summary_table[key]['known']['downirs']) + len(summary_table[key]['known']['upirs']),
                          len(summary_table[key]['novel']['downirs']) + len(summary_table[key]['novel']['upirs'])))

    k = [summary_table[key]['known']['downirs'] for key in ks if type(key) == int] + [summary_table[key]['known']['upirs']
                                                                                      for key in ks if type(key) == int]
    n = [summary_table[key]['novel']['downirs'] for key in ks if type(key) == int] + [summary_table[key]['novel']['upirs']
                                                                                      for key in ks if type(key) == int]
    handle.write('& %d/%d' % (len(set.union(*k)), len(set.union(*n))))
    handle.write('\\\\')
    handle.write('\n')

    # upregulated
    handle.write('& Up')
    for key in ks:
        handle.write(" & %d/%d" % (len(summary_table[key]['known']['upgenes']),
                                   len(summary_table[key]['novel']['upgenes'])))
    k = [summary_table[key]['known']['upgenes'] for key in ks]
    n = [summary_table[key]['novel']['upgenes'] for key in ks]

    handle.write('& %d/%d' % (len(set.union(*k)), len(set.union(*n))))
    handle.write('\\\\')
    handle.write('\n')

    # downregulated
    handle.write('& Down')
    for key in ks:
        handle.write(" & %d/%d" % (len(summary_table[key]['known']['downgenes']),
                                   len(summary_table[key]['novel']['downgenes'])))
    k = [summary_table[key]['known']['downgenes'] for key in ks]
    n = [summary_table[key]['novel']['downgenes'] for key in ks]
    handle.write('& %d/%d' % (len(set.union(*k)), len(set.union(*n))))
    handle.write('\\\\')
    handle.write('\n')

    # both
    handle.write('% Tot')
    for key in ks:
        handle.write(
            " & %d/%d" % (len(summary_table[key]['known']['downgenes']) + len(summary_table[key]['known']['upgenes']),
                          len(summary_table[key]['novel']['downgenes']) + len(summary_table[key]['novel']['upgenes'])))
    k = [summary_table[key]['known']['downgenes'] for key in ks] + [summary_table[key]['known']['upgenes'] for key in ks]
    n = [summary_table[key]['novel']['downgenes'] for key in ks] + [summary_table[key]['novel']['upgenes'] for key in ks]
    handle.write('& %d/%d' % (len(set.union(*k)), len(set.union(*n))))
    handle.write('\\\\')
    handle.write('\n')


def fullTexTableSE(summaryTable, odir=os.getcwd()):
    handle = open(os.path.join(odir, 'dirTableSE.tex'), 'w')
    ks = sorted(summaryTable.keys())[:-1]
    # upregulated
    handle.write('& Up')
    for key in ks:
        if type(key) != int: continue
        handle.write(" & %d" % (len(summaryTable[key]['upirs'])))

    k = [summaryTable[key]['upirs'] for key in ks if type(key) == int]

    handle.write('& %d' % (len(set.union(*k))))
    handle.write('\\\\')
    handle.write('\n')

    # downregulated
    handle.write('& Down')
    for key in ks:
        if type(key) != int: continue
        handle.write(" & %d" % (len(summaryTable[key]['downirs'])))

    k = [summaryTable[key]['downirs'] for key in ks if type(key) == int]
    handle.write('& %d' % (len(set.union(*k))))
    handle.write('\\\\')
    handle.write('\n')

    # both
    handle.write('& Tot')
    for key in ks:
        if type(key) != int: continue
        handle.write(" & %d" % (len(summaryTable[key]['downirs']) + len(summaryTable[key]['upirs'])))

    k = [summaryTable[key]['downirs'] for key in ks if type(key) == int] + [summaryTable[key]['upirs'] for key in ks if
                                                                            type(key) == int]

    handle.write('& %d' % (len(set.union(*k))))
    handle.write('\\\\')
    handle.write('\n')


def summary(geneRecords, aVals, level=0.05):
    summaryTable = {}
    for aidx in range(len(aVals)):
        novel = {'upirs': set(), 'downirs': set(),
                 'upgenes': set(), 'downgenes': set()}
        known = {'upirs': set(), 'downirs': set(),
                 'upgenes': set(), 'downgenes': set()}
        for gene in geneRecords:
            minGene = 1
            if not gene.IRGTested: continue
            for i, qvals in enumerate(gene.IRQvals):
                p = gene.IRPvals[i][aidx]
                if p < minGene:
                    minGene = p
                # significant
                if gene.IRQvals[i][aidx] < level:
                    # upregulated
                    intID = '%s_%s' % (gene.gid, i)
                    if gene.IRfc[i] > 0:
                        # known
                        if gene.retained[i]:
                            known['upirs'].add(intID)
                            known['upgenes'].add(gene.gid)
                        # novel
                        else:
                            novel['upirs'].add(intID)
                            novel['upgenes'].add(gene.gid)
                    # downregulated
                    else:
                        # known
                        if gene.retained[i]:
                            known['downirs'].add(intID)
                            known['downgenes'].add(gene.gid)

                        # novel
                        else:
                            novel['downirs'].add(intID)
                            novel['downgenes'].add(gene.gid)
            gene.minGene = minGene
        summaryTable[aVals[aidx]] = {'novel': novel, 'known': known}
    summaryTable['novel'] = {}
    summaryTable['novel']['upgenes'] = set.union(*[summaryTable[i]['novel']['upgenes'] for i in aVals])
    summaryTable['novel']['downgenes'] = set.union(*[summaryTable[i]['novel']['downgenes'] for i in aVals])
    summaryTable['novel']['upirs'] = set.union(*[summaryTable[i]['novel']['upirs'] for i in aVals])
    summaryTable['novel']['downirs'] = set.union(*[summaryTable[i]['novel']['downirs'] for i in aVals])
    summaryTable['known'] = {}
    summaryTable['known']['upgenes'] = set.union(*[summaryTable[i]['known']['upgenes'] for i in aVals])
    summaryTable['known']['downgenes'] = set.union(*[summaryTable[i]['known']['downgenes'] for i in aVals])
    summaryTable['known']['upirs'] = set.union(*[summaryTable[i]['known']['upirs'] for i in aVals])
    summaryTable['known']['downirs'] = set.union(*[summaryTable[i]['known']['downirs'] for i in aVals])

    return summaryTable


def summary_se(gene_records, a_vals, level=0.05):
    summary_table = {}
    for aidx in range(len(a_vals)):
        known = {'upirs': set(), 'downirs': set(),
                 'upgenes': set(), 'downgenes': set()}
        for gene in gene_records:
            min_gene = 1
            if not gene.SEGTested: continue
            for i, qvals in enumerate(gene.SEQvals):
                p = gene.SEPvals[i][aidx]
                if p < min_gene:
                    min_gene = p
                # significant
                if gene.SEQvals[i][aidx] < level:
                    # upregulated
                    SEID = '%s_%s' % (gene.gid, i)
                    if gene.SEfc[i] > 0:
                        known['upirs'].add(SEID)
                        known['upgenes'].add(gene.gid)

                        # downregulated
                    else:
                        known['downirs'].add(SEID)
                        known['downgenes'].add(gene.gid)
            gene.minGene = min_gene
        summary_table[a_vals[aidx]] = known

    summary_table['known'] = {}
    summary_table['known']['upgenes'] = set.union(*[summary_table[i]['upgenes'] for i in a_vals])
    summary_table['known']['downgenes'] = set.union(*[summary_table[i]['downgenes'] for i in a_vals])
    summary_table['known']['upirs'] = set.union(*[summary_table[i]['upirs'] for i in a_vals])
    summary_table['known']['downirs'] = set.union(*[summary_table[i]['downirs'] for i in a_vals])

    return summary_table


def plot_results(gene_records, labels, nspace, gene_model, use_log=True, odir=os.getcwd()):
    for gene in gene_records:
        if not gene.IRGTested: continue
        plotme = False
        highlights = []
        for i in range(len(gene.introns)):
            s, e = gene.introns[i]
            if min(gene.IRQvals[i]) < 0.05:
                highlights.append((s, e))
                plotme = True
        if plotme:
            f1Depths, f2Depths, f1Juncs, f2Juncs = get_depths_from_bamfiles(gene,
                                                                            nspace.factor1bamfiles,
                                                                            nspace.factor2bamfiles)
            depths = f1Depths.tolist() + f2Depths.tolist()
            plotDepth(gene, depths, labels,
                      highlights,
                      os.path.join(odir, gene.gid + '.pdf'), use_log,
                      gene_model, nspace.shrink_introns)


def plotList(geneRecords, geneDict, labels, nspace, geneModel, useLog=True, odir=os.getcwd()):
    for gene in geneRecords:
        if gene.gid.lower()[:-1] not in geneDict: continue
        highlights = []
        for s, e in geneDict[gene.gid.lower()[:-1]]:
            highlights.append((s - gene.minpos, e - gene.minpos))

        if True:
            f1Depths, f2Depths, f1Juncs, f2Juncs = get_depths_from_bamfiles(gene,
                                                                            nspace.factor1bamfiles,
                                                                            nspace.factor2bamfiles
                                                                            )
            depths = f1Depths.tolist() + f2Depths.tolist()
            plotDepth(gene, depths, labels,
                      highlights,
                      os.path.join(odir, gene.gid + '.pdf'), useLog,
                      geneModel, nspace.shrink_introns)


def plotResultsSE(geneRecords, labels, nspace, geneModel, useLog=True, odir=os.getcwd()):
    for gene in geneRecords:
        if not gene.SEGTested: continue
        plotme = False
        highlights = []
        for i in range(len(gene.flavorDict['SE'])):
            s, e = gene.flavorDict['SE'][i][1]
            if min(gene.SEQvals[i]) < nspace.fdrlevel:
                highlights.append((s, e - 1))
                plotme = True

        if plotme:
            f1Depths, f2Depths, f1Juncs, f2Juncs = get_depths_from_bamfiles(gene,
                                                                            nspace.factor1bamfiles,
                                                                            nspace.factor2bamfiles
                                                                            )
            depths = f1Depths.tolist() + f2Depths.tolist()
            plotDepth(gene, depths, labels,
                      highlights,
                      os.path.join(odir, gene.gid + '.pdf'), useLog,
                      geneModel, nspace.shrink_introns, hcolor="#00EE00")


def setValue(argv, key, cfgValue, default):
    """Simple method that performs the logic for assigning a value to a
    parameter.  If there is no configuration value, returns the default.
    If there is a configuration value, the result depends on whether the
    user entered something on the command line."""
    if not cfgValue: return default
    return default if key in argv else cfgValue


def plotDepth_old(geneM, depths, depthIDs, highlights, outname, de, geneModel, shrink_introns=False, hcolor='red'):
    """
    Plot depth for a gene, highlighting differential splicing events
    """
    gene = geneM.gene
    verbose = False
    rc('text', usetex=False)
    minPos = geneM.origGraph.minpos
    maxPos = geneM.origGraph.maxpos
    strand = gene.strand
    maxY = max([max(x) for x in depths])

    plt.figure(frameon=False)
    X = range(minPos, maxPos + 1)
    titlePadding = get_title_padding(16)
    topLine = 0.99 - titlePadding
    c = len(depths)
    if geneM.predicted: c += 1

    height = topLine * (1.0 / (1 + c) - titlePadding)
    patchDict = {}
    # Plot gene model 
    curAxes = plt.axes([AXIS_LEFT, topLine - height, AXIS_WIDTH, height])
    topLine = topLine - height - titlePadding

    # plot annotated graph
    graph = make_splice_graph(geneM.gene)
    graph.annotate()
    GeneView(geneM.gene, curAxes).plot()
    SpliceGraphView(graph, curAxes, xLimits=(minPos, maxPos)).plot()
    xlim = curAxes.get_xlim()
    xticks = setXticks(int(min(xlim)), int(max(xlim)))
    if shrink_introns:
        ranges = get_graph_ranges(gene_model_to_splice_graph(gene))
        introns = [(adjust_position(x[0] + gene.minpos, ranges) - gene.minpos + 1,
                    adjust_position(x[1] + gene.minpos, ranges) - gene.minpos + 1) for x in geneM.introns]
        highlights = [(adjust_position(x[0] + gene.minpos, ranges) - gene.minpos + 1,
                       adjust_position(x[1] + gene.minpos, ranges) - gene.minpos + 1) for x in highlights]
        for r in ranges:
            r.minpos -= gene.minpos
            r.maxpos -= gene.minpos
    else:
        introns = [(x[0], x[1]) for x in geneM.introns]
        exons = geneM.exons

    if geneM.predicted:
        curAxes = plt.axes([AXIS_LEFT, topLine - height, AXIS_WIDTH, height])
        topLine = topLine - height - titlePadding
        GeneView(gene, curAxes).plot()
        SpliceGraphView(graph, curAxes, xLimits=(minPos, maxPos), unresolved=True).plot()

    # plot depths
    for i in range(len(depths)):
        curAxes = plt.axes([AXIS_LEFT, topLine - height, AXIS_WIDTH, height])
        GeneView(gene, curAxes).plot()
        curAxes.set_ylim((0, maxY))
        if de or shrink_introns:
            curAxes.set_yscale('log', nonposy='clip')
            curAxes.set_ylim((.3, maxY))
        depth = adjust_depths(depths[i], ranges) if shrink_introns else depths[i]
        curAxes.fill_between(X, 0, depth,
                             facecolor='grey', edgecolor='grey',
                             alpha=0.75)
        for intron in introns:
            fcol = '0.65'
            s, e = intron
            curAxes.fill_between(X[s:e], 0, depth[s:e],
                                 facecolor=fcol, edgecolor=fcol, alpha=1)
        for region in highlights:
            s, e = region
            curAxes.fill_between(X[s:e], 0, depth[s:e],
                                 facecolor=hcolor, edgecolor=hcolor, alpha=1)
        for s, e in geneM.exonsI:
            curAxes.fill_between(X[s:e + 1], 0, depth[s:e + 1],
                                 facecolor='0.45', edgecolor='0.45', alpha=1)

        curAxes.set_xticks(xticks)
        curAxes.set_xticklabels([])
        topLine = topLine - height - titlePadding
        curAxes.set_xlim(xlim)

        curAxes.set_title(depthIDs[i])
    # plotLegend(patchDict)
    curAxes.set_xticklabels(xticks)
    sys.stderr.write('plotting ' + outname)
    plt.savefig(outname, dpi=400)
    plt.close()


def plotDepth(geneM, depths, depthIDs, highlights, outname, de, geneModel, shrink_introns=False, hcolor='red'):
    """
    Plot depth for a gene, highlighting IR events
    """
    verbose = False
    rc('text', usetex=False)
    gene = geneM.gene
    minPos = geneM.minpos
    maxPos = geneM.maxpos
    X = range(minPos, maxPos + 1)

    strand = gene.strand
    maxY = max([max(x) for x in depths])

    # Main setup
    cf = ConfigParser.ConfigParser()
    cf.add_section('SinglePlotConfig')
    cf.set('SinglePlotConfig', 'legend', 'True')
    cf.set('SinglePlotConfig', 'output_file', outname)
    cf.set('SinglePlotConfig', 'height', '8.0')
    cf.set('SinglePlotConfig', 'width', '16.0')
    cf.set('SinglePlotConfig', 'fontsize', '16')
    cf.set('SinglePlotConfig', 'shrink_introns', str(shrink_introns))

    cf.add_section('GeneModel')
    cf.set('GeneModel', 'plot_type', 'gene')
    cf.set('GeneModel', 'hide', 'False')
    cf.set('GeneModel', 'file_format', 'gene_model')
    cf.set('GeneModel', 'gene_name', geneM.gid)
    cf.set('GeneModel', 'source_file', 'none.gtf')
    cf.set('GeneModel', 'title_string', 'Gene Model for %s' % geneM.gid)

    config = SinglePlotConfig()
    config.config = cf
    config.validate()
    config.instantiate()
    displayList = config.getPlotList()
    plotConfig = config.getConfiguration()
    matplotlib.use('agg')

    displayList[0].source_file = 'none.gtf'
    plt.figure(frameon=False)

    gene_specs = load_gene_data(displayList, plotConfig, models={'none.gtf': geneModel}, verbose=verbose)

    width = setValue(sys.argv, '-W', plotConfig.width, DEFAULT_WIDTH)
    height = setValue(sys.argv, '-H', plotConfig.height, DEFAULT_HEIGHT)
    fontsize = setValue(sys.argv, '-F', plotConfig.fontsize, DEFAULT_FONT)
    gene_specs.setFontSize(fontsize)

    initialize_plots(gene_specs, width, height, displayList)
    titlePadding = get_title_padding(fontsize)
    topLine = 0.99 - titlePadding
    c = 2 if geneM.predicted else 1
    height = topLine * (1.0 / (len(depths) + c) - titlePadding)
    patchDict = {}
    # Plot gene model 
    curAxes = plt.axes([AXIS_LEFT, topLine - height, AXIS_WIDTH, height])
    topLine = topLine - height - titlePadding
    patches, ignore = generate_plot(displayList[0], gene_specs, curAxes,
                                   shrink=plotConfig.shrink_introns,
                                   xTickLabels=False,
                                   verbose=True)
    patchDict.update(patches)
    curAxes.set_yticks([])
    xlim = curAxes.get_xlim()
    xticks = setXticks(int(min(xlim)), int(max(xlim)))

    curAxes.set_xticklabels([])

    # plot prediction
    if geneM.predicted:
        curAxes = plt.axes([AXIS_LEFT, topLine - height, AXIS_WIDTH, height])
        topLine = topLine - height - titlePadding
        GeneView(gene_specs.gene, curAxes).plot()
        patches, extra = plot_splice_graph(geneM.origGraph, curAxes,
                                         geneName=geneM.gid,
                                         genes=[],
                                         title='Predicted Graph',
                                         xLimits=[gene_specs.gene.minpos, gene_specs.gene.maxpos],
                                         minwidth=displayList[0].edge_weight,
                                         xLabels=displayList[0].x_labels
                                         )
        patchDict.update(patches)

        curAxes.set_yticks([])
        curAxes.set_xlim(xlim)
    # xticks = setXticks(int(min(xlim)), int(max(xlim)))

    curAxes.set_xticklabels([])

    if shrink_introns:
        ranges = get_graph_ranges(gene_model_to_splice_graph(gene),
                                  scaleFactor=plotConfig.shrink_factor)
        introns = [(adjust_position(x[0] + gene.minpos, ranges) - gene.minpos + 1,
                    adjust_position(x[1] + gene.minpos, ranges) - gene.minpos + 1) \
                   for x in geneM.introns]
        highlights = [(adjust_position(x[0] + gene.minpos, ranges) - gene.minpos + 1,
                       adjust_position(x[1] + gene.minpos, ranges) - gene.minpos + 1) \
                      for x in highlights]
        for r in ranges:
            r.minpos -= gene.minpos
            r.maxpos -= gene.minpos
    else:
        introns = [(x[0], x[1]) for x in geneM.introns]
        exons = geneM.exons

    # plot depths
    for i in range(len(depths)):
        curAxes = plt.axes([AXIS_LEFT, topLine - height, AXIS_WIDTH, height])
        GeneView(gene_specs.gene, curAxes).plot()
        curAxes.set_ylim((0, maxY))
        if de:
            curAxes.set_yscale('log', nonposy='clip')
            curAxes.set_ylim((.3, maxY))
        depth = adjust_depths(depths[i], ranges) if shrink_introns else depths[i]

        #        for s,e in exons:
        #        curAxes.fill_between( X[s:e+1],  0, depth[s:e+1],
        #                                  facecolor='grey', alpha=0.5)
        curAxes.fill_between(X, 0, depth,
                             facecolor='grey', edgecolor='grey',
                             alpha=0.75)
        for intron in introns:
            fcol = '0.65'
            s, e = intron
            curAxes.fill_between(X[s:e], 0, depth[s:e],
                                 facecolor=fcol, edgecolor=fcol, alpha=1)
        for region in highlights:
            s, e = region
            curAxes.fill_between(X[s:e], 0, depth[s:e],
                                 facecolor=hcolor, edgecolor=hcolor, alpha=1)
        for s, e in geneM.exonsI:
            curAxes.fill_between(X[s:e + 1], 0, depth[s:e + 1],
                                 facecolor='0.45', edgecolor='0.45', alpha=1)

        curAxes.set_xticks(xticks)
        curAxes.set_xticklabels([])
        topLine = topLine - height - titlePadding
        curAxes.set_xlim(xlim)

        curAxes.set_title(depthIDs[i])
    # plotLegend(patchDict)
    curAxes.set_xticklabels(xticks)
    plt.savefig(outname, dpi=400)
    plt.close()


def plot_reduced_graph(gene_model, odir=os.getcwd(), ext='png', **args):
    """
    Plot original and reduced splice graphs
    """
    reduced_graph = gene_model.graph
    original_grapn = gene_model_to_splice_graph(gene_model.gene)

    verbose = args.get('verbose', False)

    rc('text', usetex=False)
    gene = gene_model.gene
    gid = gene_model.gid
    minPos = min(gene.start(), gene.end())
    maxPos = max(gene.start(), gene.end())
    strand = gene.strand

    # Main setup
    cf = ConfigParser.ConfigParser()
    cf.add_section('SinglePlotConfig')
    cf.set('SinglePlotConfig', 'legend', 'True')
    cf.set('SinglePlotConfig', 'output_file', '')
    cf.set('SinglePlotConfig', 'height', '8.0')
    cf.set('SinglePlotConfig', 'width', '16.0')
    cf.set('SinglePlotConfig', 'fontsize', '16')
    cf.set('SinglePlotConfig', 'shrink_introns', 'False')

    # Gene model
    cf.add_section('GeneModel')
    cf.set('GeneModel', 'plot_type', 'gene')
    cf.set('GeneModel', 'hide', 'False')
    cf.set('GeneModel', 'file_format', 'gene_model')
    cf.set('GeneModel', 'gene_name', gid)
    cf.set('GeneModel', 'source_file', 'none.gtf')
    cf.set('GeneModel', 'title_string', 'Gene Model for %s' % gid)

    config = SinglePlotConfig()
    config.config = cf
    config.validate()
    config.instantiate()
    displayList = config.getPlotList()
    plotConfig = config.getConfiguration()
    matplotlib.use('agg')

    displayList[0].source_file = 'none.gtf'

    plt.figure(frameon=False)

    geneSpecs = load_gene_data(displayList,
                               plotConfig, models={'none.gtf': gene_model, 'none2.gtf': reduced_graph},
                               verbose=verbose)

    width = setValue(sys.argv, '-W', plotConfig.width, DEFAULT_WIDTH)
    height = setValue(sys.argv, '-H', plotConfig.height, DEFAULT_HEIGHT)
    fontsize = setValue(sys.argv, '-F', plotConfig.fontsize, DEFAULT_FONT)
    geneSpecs.setFontSize(fontsize)

    initialize_plots(geneSpecs, width, height, displayList)
    titlePadding = get_title_padding(fontsize)
    topLine = 0.99 - titlePadding
    height = topLine * (1.0 / 2 - titlePadding)
    patchDict = {}
    # Plot gene model
    curAxes = plt.axes([AXIS_LEFT, topLine - height, AXIS_WIDTH, height])
    topLine = topLine - height - titlePadding
    geneSpecs.gene = gene_model.ogene
    patches, ignore = generate_plot(displayList[0], geneSpecs, curAxes,
                                    shrink=plotConfig.shrink_introns,
                                    xTickLabels=False,
                                    verbose=True)
    patchDict.update(patches)
    curAxes.set_yticks([])
    xlim = curAxes.get_xlim()
    xticks = setXticks(int(min(xlim)), int(max(xlim)))

    curAxes.set_xticklabels([])
    availHeight = 0.45 * get_available_height(displayList, fontsize, legend=True)
    height = availHeight * displayList[0].relative_size

    # Plot reduced graph
    curAxes = plt.axes([AXIS_LEFT, topLine - height, AXIS_WIDTH, height])
    GeneView(gene_model.ogene, curAxes).plot()
    patches, extra = plot_splice_graph(reduced_graph, curAxes,
                                       geneName=gid,
                                       genes=[],
                                       title='Reduced Graph',
                                       xLimits=[geneSpecs.gene.minpos, geneSpecs.gene.maxpos],
                                       minwidth=displayList[0].edge_weight,
                                       xLabels=displayList[0].x_labels
                                       )
    patchDict.update(patches)
    plot_legend(patchDict)
    curAxes.set_yticks([])
    curAxes.set_xlim(xlim)
    # xticks = setXticks(int(min(xlim)), int(max(xlim)))

    curAxes.set_xticklabels([])
    outname = os.path.join(odir, gid + '.%s' % ext)
    plt.savefig(outname, dpi=400)
    plt.close()


def plot_statistic(gene_records, nbins=20, odir=os.getcwd(), ext='pdf'):
    X = list(chain.from_iterable([numpy.array(gene_records[i].intFC) / \
                                  gene_records[i].serr \
                                  for i in range(len(gene_records))]))
    fig = plt.figure()
    plot = fig.add_subplot(111)
    plot.tick_params(axis='both', which='major', labelsize=16)
    plot.tick_params(axis='both', which='minor', labelsize=16)

    plt.hist(X, bins=nbins)
    plt.savefig(os.path.join(odir, 'figures', 'stat.%s') % (ext))
    plt.close()


def plot_p_dist(gene_records, odir=os.getcwd(), nbins=20, ext='pdf'):
    """
    Plot p-value distribution
    """
    pvals = []
    for gene in gene_records:
        if not gene.IRGTested: continue
        for i, IRPvals in enumerate(gene.IRPvals):
            if gene.IRTested[i]:
                pvals.append(numpy.min(IRPvals))

    fig = plt.figure()
    plot = fig.add_subplot(111)
    plot.tick_params(axis='both', which='major', labelsize=16)
    plot.tick_params(axis='both', which='minor', labelsize=16)
    plt.hist(pvals, bins=nbins)
    plt.savefig(os.path.join(odir, 'pvalues.%s') % (ext))
    plt.close()


def plot_p_dist_se(gene_records, odir=os.getcwd(), nbins=20, ext='pdf'):
    """
    Plot p-value distribution
    """
    pvals = []
    for gene in gene_records:
        if not gene.SEGTested: continue
        for i, SEPvals in enumerate(gene.SEPvals):
            if gene.SETested[i]:
                pvals.append(numpy.min(SEPvals))

    fig = plt.figure()
    plot = fig.add_subplot(111)
    plot.tick_params(axis='both', which='major', labelsize=16)
    plot.tick_params(axis='both', which='minor', labelsize=16)
    plt.hist(pvals, bins=nbins)
    plt.savefig(os.path.join(odir, 'pvaluesSE.%s') % (ext))
    plt.close()


def plotMVAold(geneRecords, odir=os.getcwd(), ext='pdf'):
    """
    Render MA plot
    """
    fig = plt.figure(figsize=(8, 8), dpi=200)

    plot = fig.add_subplot(111)
    plot.tick_params(axis='both', which='major', labelsize=12)
    plot.tick_params(axis='both', which='minor', labelsize=12)

    pvals = list(chain.from_iterable([numpy.array([geneRecords[i].intPvals[t] for t in range(
        len(geneRecords[i].intPvals)) \
                                                   if geneRecords[i].intTested[t]]) \
                                      for i in range(len(geneRecords))]))

    N = len(pvals)
    M = list(chain.from_iterable([numpy.array([geneRecords[i].intFCr[t] for t in range(
        len(geneRecords[i].intPvals)) \
                                               if geneRecords[i].intTested[t]]) \
                                  for i in range(len(geneRecords))]))

    A = list(chain.from_iterable([numpy.array([geneRecords[i].intExp[t] for t in range(
        len(geneRecords[i].intPvals)) \
                                               if geneRecords[i].intTested[t]]) \
                                  for i in range(len(geneRecords))]))

    # plot background points
    CA = [A[i] for i in range(0, N) if pvals[i] >= 0.05 and abs(M[i]) < 1]
    CM = [M[i] for i in range(0, N) if pvals[i] >= 0.05 and abs(M[i]) < 1]
    plt.plot(CA, CM, color='0.65', marker='.', linestyle='')

    CA = [A[i] for i in range(0, N) if pvals[i] >= 0.05 and abs(M[i]) >= 1]
    CM = [M[i] for i in range(0, N) if pvals[i] >= 0.05 and abs(M[i]) >= 1]
    plt.plot(CA, CM, color='0.65', marker='.', linestyle='')

    # plot pvalue 0.05  points
    CA = [A[i] for i in range(N) if pvals[i] < 0.05 and pvals[i] >= 0.001]
    CM = [M[i] for i in range(N) if pvals[i] < 0.05 and pvals[i] >= 0.001]
    plt.plot(CA, CM, 'k.', label='0.05')

    # plot pvalue 0.05  points
    CA = [A[i] for i in range(N) if pvals[i] < 0.001]
    CM = [M[i] for i in range(N) if pvals[i] < 0.001]

    plt.plot(CA, CM, 'r.', label='0.05')

    plt.xlabel(r'$\frac{1}{2}(\log_2 \hat{f}_1 + \log_2 \hat{f}_2)$', size=18)
    plt.ylabel(r'$\log_{2}\hat{f}_1 - \log_2\hat{f}_2$', size=18)
    plt.grid()
    plt.legend()
    plt.savefig(os.path.join(odir, 'figures', 'mva.%s') % (ext))


def plot_mva(gene_records, a_vals, odir=os.getcwd(), ext='pdf'):
    """
    Render MA plot
    """
    plt.clf()
    plt.cla()
    plt.autoscale(True)
    colors = ['blue', 'green', 'brown', 'cyan', 'orange', 'olive', 'pink', 'yellow', 'black',
              'SpringGreen', 'Coral']
    fig = plt.figure()
    plot = fig.add_subplot(111)
    plot.tick_params(axis='both', which='major', labelsize=12)
    plot.tick_params(axis='both', which='minor', labelsize=12)

    # plot background surface
    nsigfcs = []
    nsigexps = []

    for gene in gene_records:
        if not gene.IRGTested: continue
        for i, qvals in enumerate(gene.IRQvals):
            if gene.IRTested[i] and numpy.min(qvals) >= 0.0:
                nsigfcs.append(gene.IRfc[i])
                nsigexps.append(gene.IRexp[i])

    H, xedges, yedges = np.histogram2d(nsigfcs, nsigexps, bins=(20, 10))
    ydiff = 0  # abs(yedges[0]-yedges[1])
    xdiff = 0  # abs(xedges[0]-xedges[1])

    extent = [min(nsigexps) + ydiff, max(nsigexps) + ydiff, -max(nsigfcs), -min(nsigfcs)]

    plt.imshow(H, extent=extent, interpolation='gaussian', origin='upper', cmap=plt.get_cmap('binary'), alpha=0.5,
               rasterized=0, vmax=np.max(H) / 25.0, aspect='equal')
    plt.autoscale(False)
    plt.axhline(0, ls='--', lw=1, color='r')
    for aidx in range(len(a_vals)):
        M = []
        A = []
        for gene in gene_records:
            if not gene.IRGTested: continue
            for i, qvals in enumerate(gene.IRQvals):
                if gene.IRTested[i] and numpy.argmin(qvals) == aidx and numpy.min(qvals) < 0.05:
                    M.append(gene.IRfc[i])
                    A.append(gene.IRexp[i])

        plt.plot(A, M, color=colors[aidx], marker='.', linestyle='', label=r'$a = 2^{%s}$' % a_vals[aidx])

    plt.xlabel(r'$\frac{1}{2}(\log_2 \hat{f}_1 + \log_2 \hat{f}_2)$', size=18)
    plt.ylabel(r'$\log_{2}\hat{f}_1 - \log_2\hat{f}_2$', size=18)

    # Shrink current axis by 20%
    box = plot.get_position()
    plot.set_position([box.x0, box.y0, box.width * 0.8, box.height])

    # Put a legend to the right of the current axis
    plot.legend(loc='center left', bbox_to_anchor=(1, 0.5), fancybox=True, shadow=True)
    plot.grid()
    plt.savefig(os.path.join(odir, 'mva.%s') % (ext))
    plt.autoscale(True)
    plt.close()


def plotMVASE(geneRecords, aVals, odir=os.getcwd(), ext='pdf'):
    """
    Render MA plot
    """
    fig = plt.figure(figsize=(8, 8), dpi=200)

    plot = fig.add_subplot(111)
    plot.tick_params(axis='both', which='major', labelsize=12)
    plot.tick_params(axis='both', which='minor', labelsize=12)
    colors = ['blue', 'green', 'brown', 'cyan', 'orange', 'olive', 'pink', 'yellow', 'black',
              'SpringGreen', 'Coral']

    pvals = list(chain.from_iterable([numpy.array([geneRecords[i].SEPvals[t] for t in range(
        len(geneRecords[i].SEPvals)) \
                                                   if geneRecords[i].SETested[t]]) \
                                      for i in range(len(geneRecords)) if geneRecords[i].SEGTested]))

    N = len(pvals)
    M = list(chain.from_iterable([numpy.array([geneRecords[i].SEfc[t] for t in range(
        len(geneRecords[i].SEPvals)) \
                                               if geneRecords[i].SETested[t]]) \
                                  for i in range(len(geneRecords)) if geneRecords[i].SEGTested]))

    A = list(chain.from_iterable([numpy.array([geneRecords[i].SEexp[t] for t in range(
        len(geneRecords[i].SEPvals)) \
                                               if geneRecords[i].SETested[t]]) \
                                  for i in range(len(geneRecords)) if geneRecords[i].SEGTested]))

    # plot background points
    plt.plot(A, M, color='0.65', marker='.', linestyle='')

    # plot significat SEs
    for aidx in range(len(aVals)):
        M = []
        A = []
        for gene in geneRecords:
            if not gene.SEGTested: continue
            for i, qvals in enumerate(gene.SEQvals):
                if gene.SETested[i] and numpy.argmin(qvals) == aidx and numpy.min(qvals) < 0.05:
                    M.append(gene.SEfc[i])
                    A.append(gene.SEexp[i])

        plt.plot(A, M, color=colors[aidx], marker='.', linestyle='', label=r'$a = 2^{%s}$' % aVals[aidx])

    plt.xlabel(r'$\frac{1}{2}(\log_2 \hat{f}_1 + \log_2 \hat{f}_2)$', size=18)
    plt.ylabel(r'$\log_{2}\hat{f}_1 - \log_2\hat{f}_2$', size=18)
    plt.grid()
    # Shrink current axis by 20%
    box = plot.get_position()
    plot.set_position([box.x0, box.y0, box.width * 0.8, box.height])

    # Put a legend to the right of the current axis
    plot.legend(loc='center left', bbox_to_anchor=(1, 0.5), fancybox=True, shadow=True)

    plt.savefig(os.path.join(odir, 'mvaSE.%s') % (ext))


def plotMVASE_smear(geneRecords, aVals, odir=os.getcwd(), ext='pdf'):
    """
    Render MA plot
    """
    plt.clf()
    plt.cla()
    plt.autoscale(True)
    colors = ['blue', 'green', 'brown', 'cyan', 'orange', 'olive', 'pink', 'yellow', 'black',
              'SpringGreen', 'Coral']
    fig = plt.figure()
    plot = fig.add_subplot(111)
    plot.tick_params(axis='both', which='major', labelsize=12)
    plot.tick_params(axis='both', which='minor', labelsize=12)

    # plot background surface
    nsigfcs = []
    nsigexps = []

    for gene in geneRecords:
        for i, qvals in enumerate(gene.SEQvals):
            if gene.SETested[i] and numpy.min(qvals) >= 0.0:
                nsigfcs.append(gene.SEfc[i])
                nsigexps.append(gene.SEexp[i])

    H, xedges, yedges = np.histogram2d(nsigfcs, nsigexps, bins=(30, 30))
    ydiff = 0  # abs(yedges[0]-yedges[1])
    xdiff = 0  # abs(xedges[0]-xedges[1])

    extent = [min(nsigexps), max(nsigexps), -max(nsigfcs), -min(nsigfcs)]

    plt.imshow(H, extent=extent, interpolation='gaussian', origin='upper', cmap=plt.get_cmap('binary'), alpha=1,
               rasterized=0, vmax=np.max(H) / 25.0, aspect=1)
    plt.autoscale(False)
    plt.axhline(0, ls='--', lw=1, color='r')
    for aidx in range(len(aVals)):
        M = []
        A = []
        for gene in geneRecords:
            for i, qvals in enumerate(gene.SEQvals):
                if gene.SETested[i] and numpy.argmin(qvals) == aidx and numpy.min(qvals) < 0.05:
                    M.append(gene.SEfc[i])
                    A.append(gene.SEexp[i])

        plt.plot(A, M, color=colors[aidx], marker='.', linestyle='', label=r'$a = 2^{%s}$' % aVals[aidx])

    plt.xlabel(r'$\frac{1}{2}(\log_2 \hat{f}_1 + \log_2 \hat{f}_2)$', size=18)
    plt.ylabel(r'$\log_{2}\hat{f}_1 - \log_2\hat{f}_2$', size=18)
    plt.legend(ncol=2)
    plt.savefig(os.path.join(odir, 'mvaSE.%s') % (ext))
    plt.autoscale(True)
    plt.close()
