#!/usr/bin/env python

#
#    iDiffIR- identifying differential intron retention (IR) from RNA-seq
#    Copyright (C) 2015  Michael Hamilton
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Main iDiffIR script

:author: Mike Hamilton
"""
import os
import sys
import pysam
import numpy
import matplotlib
from argparse import ArgumentParser

from SpliceGrapher.formats.loader import load_gene_models

from iDiffIR.IntronModel import make_models
from iDiffIR.Plot import (summary, summary_se, full_tex_table, write_lists, writeAll, plot_p_dist, plot_mva, plot_results,
                          fullTexTableSE, write_all_se, plot_p_dist_se, plotMVASE, plotResultsSE, )
from iDiffIR.Stat import computeIRStatistics, computeSEStatistics
from iDiffIR.BamfileIO import get_depths_from_bamfiles

matplotlib.use('agg')


def file_list(raw):
    """Converts a **:**-separated string of file paths into a list
    
    Parameters
    ----------
    raw : string containing paths separated by a **:**

    Returns
    -------
    p : list
        List of file paths
        
    """
    return [r.strip() for r in raw.strip().split(':')]


def _validate_bamfiles(bamfile_list):
    """Check if bamfiles exist

    Parameters
    ----------
    bamfile_list : List of paths to check

    Returns
    -------
    b : bool
        True if all files exist at given paths

    """
    bamfilesOK = True
    for f in bamfile_list:
        if not os.path.exists(f):
            sys.stderr.write('**bamfile %s not found\n' % f)
    return bamfilesOK


def validate_args(nspace):
    """Verify program arguments

    Checks all arguments to **idiffir.py** for consistency
    and feasibilty.

    Parameters
    ----------
    nspace : argparse.Namespace object containing **idiffir.py** arguments
    
    .. todo:: add rest of argument checks
 
    Returns
    -------
    b : bool
        True if parameters are feasible

    """
    gene_model_ok = True
    c_param_ok = True

    # bamfiles
    cfOK1 = _validate_bamfiles(nspace.factor1bamfiles)
    cfOK2 = _validate_bamfiles(nspace.factor2bamfiles)
    countFilesOK = cfOK1 and cfOK2

    # gene model
    if not os.path.isfile(nspace.genemodel):
        sys.stderr.write('**Gene model file %s not found\n' % nspace.genemodel)
        gene_model_ok = False

    # check event coverage parameter
    if nspace.coverage < 0 or nspace.coverage > 1:
        sys.stderr.write('Feasible values of coverage filter `-c`, `--coverage`: 0 <= c <= 1\n')
        c_param_ok = False

    return countFilesOK and c_param_ok and gene_model_ok


def make_output_dir(nspace):
    """Make output directories

    Make output directories and subdirectories for **iDiffIR** output

    Parameters
    ----------
    nspace : argparse.Namespace object with **idiffir.py** parameters

    Returns
    -------
    status : bool
             True if directories were able to be created or already exist.
    """
    try:
        # build main directory
        if not os.path.exists(nspace.outdir):
            os.makedirs(nspace.outdir)

        # build figures subdirectory
        if not os.path.exists(os.path.join(nspace.outdir, 'figures')) and not nspace.noplot:
            os.makedirs(os.path.join(nspace.outdir, 'figures'))

        # build figuresLog subdirectory
        if not os.path.exists(os.path.join(nspace.outdir, 'figuresLog')) and not nspace.noplot:
            os.makedirs(os.path.join(nspace.outdir, 'figuresLog'))

        # HTML output not implemented yet
        # # build HTML directory
        # if not os.path.exists(os.path.join(nspace.outdir, 'HTML')):
        #     os.makedirs(os.path.join(nspace.outdir, 'HTML'))

        # build lists subdirectory
        if not os.path.exists(os.path.join(nspace.outdir, 'lists')):
            os.makedirs(os.path.join(nspace.outdir, 'lists'))

    # couldn't make a directory...
    except OSError:
        return False
    # all ok
    return True


def write_status(status, verbose=False):
    """Print status message
    
    Pretty-print status message to stdout

    Parameters
    ----------
    status  : string to write
    verbose : whether to display status

    """
    # do nothing if we're keeping quiet
    if not verbose: return

    # print otherwise
    n = len(status) + 2
    sys.stderr.write('%s\n' % ('-' * n))
    sys.stderr.write(' %s \n' % status)
    sys.stderr.write('%s\n' % ('-' * n))


def run_intron(gene_records, gene_model, nspace, valid_chroms, f1L_norm, f2L_norm):
    """Run differential IR analysis

    Run **iDiffIR** differential intron retention analysis.

    Parameters
    ----------
    gene_records : list of IntronModel.IntronModel objects
    gene_model   : SpliceGrapher.formats.GeneModel.GeneModel
                  Gene model object for the provided genome
    nspace      : argparse.ArgumentParser 
                  Command line arguments for **idiffir.py**
    
    """
    # deprecated bias adjustment
    #    if nspace.biasAdj:
    #        f1Codes, f2Codes, f1Cnt, f2Cnt = removePositionalBias(
    #            geneRecords, f1Dict, f1Dict, nspace.numClusts)
    #        rmi = rmsip(geneRecords, f1Dict, f2Dict )
    #        plotGBCs( f1Codes, f2Codes, f1Cnt, f2Cnt, rmi, nspace.outdir)
    a_vals = range(nspace.krange[0], nspace.krange[1] + 1)
    # compute differential IR statistics
    write_status('Computing statistics', nspace.verbose)
    computeIRStatistics(gene_records, nspace, valid_chroms, f1L_norm, f2L_norm)

    # create a summary dictionary of results
    summary_dict = summary(gene_records, a_vals, nspace.fdrlevel)
    # write full latex table
    full_tex_table(summary_dict, os.path.join(nspace.outdir, 'lists'))
    # write gene lists
    write_lists(summary_dict, os.path.join(nspace.outdir, 'lists'))
    # write all IR events
    writeAll(gene_records, a_vals, os.path.join(nspace.outdir, 'lists'))
    # writeGeneExpression(geneRecords, os.path.join(nspace.outdir, 'lists'))

    # plot figures for significant events
    write_status('Plotting Depths', nspace.verbose)
    f1labs = ['%s Rep %d' % (nspace.factorlabels[0], i + 1) for i in range(len(nspace.factor1bamfiles))]
    f2labs = ['%s Rep %d' % (nspace.factorlabels[1], i + 1) for i in range(len(nspace.factor2bamfiles))]

    # finish up if we're not plotting
    if nspace.noplot:
        return

    # plot diagnostic figures (p-value distribution and MvA )
    plot_p_dist(gene_records, os.path.join(nspace.outdir, 'figures'))
    plot_mva(gene_records, a_vals, os.path.join(nspace.outdir, 'figures'))

    plot_results(gene_records, f1labs + f2labs, nspace, gene_model, False, os.path.join(nspace.outdir, 'figures'))
    plot_results(gene_records, f1labs + f2labs, nspace, gene_model, True, os.path.join(nspace.outdir, 'figuresLog'))


def run_exon(gene_records, gene_model, nspace, valid_chroms, f1L_norm, f2L_norm):
    """Run differential exon skipping analysis

    Run iDiffIR's differential exon skipping analysis

    Parameters
    ----------
    gene_records : list
                  List of IntronModel.IntronModel objects
    gene_model   : SpliceGrapher.formats.GeneModel.GeneModel
                  Gene model object for the provided genome
    nspace      : argparse.ArgumentParser 
                  Command line arguments for **idiffir.py**

    """
    a_vals = range(nspace.krange[0], nspace.krange[1] + 1)
    write_status("Computing SE statistics", nspace.verbose)
    computeSEStatistics(gene_records, nspace, valid_chroms, f1L_norm, f2L_norm)
    summary_dict_se = summary_se(gene_records, a_vals, nspace.fdrlevel)
    fullTexTableSE(summary_dict_se, os.path.join(nspace.outdir, 'lists'))
    # writeListsSE( summaryDict, os.path.join(nspace.outdir, 'lists'))
    write_all_se(gene_records, a_vals, os.path.join(nspace.outdir, 'lists'))
    write_status("Plotting Depths", nspace.verbose)
    f1labs = ['%s Rep %d' % (nspace.factorlabels[0], i + 1) \
              for i in range(len(nspace.factor1bamfiles))]
    f2labs = ['%s Rep %d' % (nspace.factorlabels[1], i + 1) \
              for i in range(len(nspace.factor2bamfiles))]

    if nspace.noplot:
        return
    plot_p_dist_se(gene_records, os.path.join(nspace.outdir, 'figures'))
    plotMVASE(gene_records, a_vals, os.path.join(nspace.outdir, 'figures'))
    plotResultsSE(gene_records, f1labs + f2labs,
                  nspace, gene_model, False,
                  os.path.join(nspace.outdir, 'figures'))
    plotResultsSE(gene_records, f1labs + f2labs,
                  nspace, gene_model, True,
                  os.path.join(nspace.outdir, 'figuresLog'))


def _dispatch(event):
    """Run differential event analysis for given event

    Dispatch differential analysis to corresponding function

    Parameters
    ----------
    event : str
            Either IR or SE indicating intron retention or exon skipping
    """
    # run differential IR analysis
    if event == 'IR':
        return run_intron

    # run differential SE analysis
    elif event == 'SE':
        return run_exon

    # invalalid event ID
    else:
        raise ValueError


def get_valid_chromosomes(nspace):
    """Find chromosomes that are covered in 
       each bamfile

    """
    f1Chroms = []
    for path in nspace.factor1bamfiles:
        bamfile = pysam.Samfile(path, 'rb')
        f1Chroms.append(set([chrom.lower() for chrom in bamfile.references]))
    f2Chroms = []
    for path in nspace.factor2bamfiles:
        bamfile = pysam.Samfile(path, 'rb')
        f2Chroms.append(set([chrom.lower() for chrom in bamfile.references]))
    f1Chroms = set.intersection(*f1Chroms)
    f2Chroms = set.intersection(*f2Chroms)
    return set.intersection(f1Chroms, f2Chroms)


def get_norm_factors(nspace):
    f1Cnts = []
    try:
        for path in nspace.factor1bamfiles:
            bamfile = pysam.Samfile(path, 'rb')
            f1Cnts.append(bamfile.mapped)
        f2Cnts = []
        for path in nspace.factor2bamfiles:
            bamfile = pysam.Samfile(path, 'rb')
            f2Cnts.append(bamfile.mapped)
        mVal = max(f1Cnts + f2Cnts)
        f1LNorm = [mVal / float(x) for x in f1Cnts]
        f2LNorm = [mVal / float(x) for x in f2Cnts]
    except ValueError:
        seps = '*' * 41
        sys.exit('%s\n Make sure bamfiles are indexed--exiting\n%s' %
                 (seps, seps))
    return f1LNorm, f2LNorm


def check_unspliced(lst, perc):
    length = len(lst)
    count = numpy.count_nonzero(lst)
    perc_calc = (float(count) / length) * 100
    # print perc_calc,count,length
    return perc_calc < perc


"""
+1 omitted since intron is (a,b) and next exon is (b,c) not (b+1,c) so maybe actual intron is (a,b-1)?
"""


def intron_len(start, end):
    return end - start


def check_gene_unspliced(gene, replicate):
    for i, r in enumerate(gene.introns):
        s, e = r
        if intron_len(s, e) < 926:  # 463.06 is mean intron length, 149.0 is median, introns with size more than 926 (twice of mean) should be checked for 90% positions
            criterion = 100  # 100 here is cirterion, all positions have read coverage, False is Unspliced, True is Spliced
        else:
            criterion = 90
        if check_unspliced(replicate[s:e], criterion):  # if true then it means this intron is not unspliced
            return False
    return True


def filter_unspliced(gene_records, nspace):
    index_track = []
    for i in range(0, len(gene_records)):
        gene = gene_records[i]
        if i % 100 == 0:
            print('filtered: ', i)
        if len(gene.introns) <= 1:  # One or Less Intron genes, ignore them (every gene should have 2 or more introns)
            continue
        comb_res = get_depths_from_bamfiles(gene, nspace.factor1bamfiles, nspace.factor2bamfiles)
        trt1, trt2 = comb_res[0]  # first depth
        ctr1, ctr2 = comb_res[1]  # second depth

        # Alternatively, pooling replicates #
        trt = trt1 + trt2
        ctr = ctr1 + ctr2

        trt_res = check_gene_unspliced(gene, trt)
        ctr_res = check_gene_unspliced(gene, ctr)

        if trt_res or ctr_res:
            index_track.append(i)

    final = numpy.delete(gene_records, index_track).tolist()
    return final


def main():
    parser = ArgumentParser(description='Identify differentially expressed introns.')
    parser.add_argument('-v', '--verbose', dest='verbose', action='store_true',
                        default=False, help="verbose output [default is quiet running]")
    parser.add_argument('-n', '--noplot', dest='noplot', action='store_true',
                        default=False, help="Do not plot figures [default is to make figures]")
    parser.add_argument('-l', '--factorlabel', dest="factorlabels", action='store', nargs=2,
                        type=str, default=['factor1', 'factor2'],
                        help="factor labels, example:  -f Mutant Wildtype", metavar='FACTORLABEL')
    parser.add_argument('-o', '--output-dir', dest='outdir', action='store',
                        default='iDiffIR_output', help="output file directory name")
    parser.add_argument('-s', '--shrink_introns', dest='shrink_introns', action='store_true',
                        default=False, help='shrink introns for depth plots [default is no shrinking]')
    parser.add_argument('-k', '--krange', dest='krange', action='store',
                        default=[2, 6], type=int, help='kmin kmax; [default is to search for kmax]', nargs=2)
    parser.add_argument('-c', '--coverage', dest='coverage', action='store', default=0.99,
                        type=float, help='coverage cutoff, default = 0.99')
    parser.add_argument('-d', '--dexpThresh', dest='dexpThresh', action='store', default=10,
                        type=float, help='differential gene expression threshold, [default = 10]')
    parser.add_argument('-p', '--procs', dest='procs', action='store', default=1,
                        type=int, help='Number of processing cores to use, [default = 1]')
    parser.add_argument('-f', '--fdrlevel', dest='fdrlevel', action='store', default=0.05,
                        type=float, help='FDR test level, [default = 0.05]')
    parser.add_argument('-g', '--graph-dirs', dest='graphDirs', type=file_list,
                        help='colon-separated list of directories to recursively search for SpliceGrapher predictions')
    parser.add_argument('-G', '--graph-dirs-only', dest='graphDirsOnly', type=file_list,
                        help='colon-separated list of directories to recursively search for SpliceGrapher predictions. \
                            In this case only the predicted graphs are used. i.e. the gene models are only used in plots \
                            and not for building reduced gene models.  Useful for poorly annotated genomes.')
    parser.add_argument('-m', '--multTest', dest='multTest', choices=['BF', 'BH', 'QV'], type=str, default='QV',
                        help='Multiple testing adjustment method BF: Bonferroni, BH: Benjamini-Hochberg, QV: q-values [default = QV]')
    parser.add_argument('-e', '--event', choices=['IR', 'SE'], type=str, default='IR',
                        help='AS event to test, IR: Intron Retention, SE: Exon Skipping [default = IR] [default is IR]')
    parser.add_argument('genemodel', type=str,
                        help="gene model file: NAME.gtf[.gz] | NAME.gff[.gz]")
    parser.add_argument('factor1bamfiles', type=file_list,
                        help="colon-separated list of bamfiles: PATH-TO-REPLICATE_1[:PATH-TO-REPLICATE_2,...]")
    parser.add_argument('factor2bamfiles', type=file_list,
                        help="colon-separated list of bamfiles: PATH-TO-REPLICATE_1[:PATH-TO-REPLICATE_2,...]")

    args = parser.parse_args()
    if not validate_args(args):
        raise Exception("Argument Errors: check arguments and usage!")

    valid_chroms = get_valid_chromosomes(args)
    f1LNorm, f2LNorm, = get_norm_factors(args)
    if not make_output_dir(args):
        sys.exit('Could not create directory: %s' % args.outdir)

    write_status('Loading models', args.verbose)
    gene_model = load_gene_models(args.genemodel, verbose=args.verbose)
    write_status('Making reduced models', args.verbose)
    gene_records = make_models(gene_model, args.outdir, verbose=args.verbose, graph_dirs=args.graphDirs,
                               exonic=args.event == 'SE', procs=args.procs)

    gene_records = filter_unspliced(gene_records, args)

    _dispatch(args.event)(gene_records, gene_model, args, valid_chroms, f1LNorm, f2LNorm)


if __name__ == "__main__":
    main()
