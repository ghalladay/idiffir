#!/bin/python

#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.

#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#
#    Author: Mike Hamilton, Colorado State University, 2013
#

from argparse import ArgumentParser
import math
import numpy
import os
import sys

from iDiffIR.IntronModel import make_models
from iDiffIR.BamfileIO import get_depths_from_bam
from SpliceGrapher.formats.loader import load_gene_models
from SpliceGrapher.formats.fasta import _fasta_itr


def file_list(raw):
    return [r.strip() for r in raw.strip().split(':')]


def validate_args(nspace):
    """
    Verify arguments
    """
    count_files_ok = True
    gene_model_ok = True

    # count files
    for f in nspace.factor1files:
        if not os.path.exists(f):
            sys.stderr.write('Counts directory %s not found' % f)
            count_files_ok = False

    # gene model
    if not os.path.isfile(nspace.genemodel):
        sys.stderr.write('File %s not found' % nspace.genemodel)
        gene_model_ok = False

    return count_files_ok and gene_model_ok


def writeStatus(status):
    """
    Pretty print status
    """
    n = len(status) + 2
    sys.stderr.write('%s\n' % ('-' * n))
    sys.stderr.write(' %s \n' % status)
    sys.stderr.write('%s\n' % ('-' * n))


def load_data(nspace, geneModel):
    """
    Load gene depths
    """
    f1Dict = {}

    def load(factorfiles, fDict):
        for chrm in sorted(geneModel.getChromosomes()):
            genes = geneModel.getGeneRecords(chrm)
            genes.sort()
            # if chrm != 'chr01':
            # break
            for i in range(len(factorfiles)):
                fname = os.path.join(factorfiles[i],
                                     '%s.cnt.gz' % chrm)
                if not os.path.exists(fname):
                    sys.stderr.write('Depths file %s not found\n' % fname)
                    continue
                itr = _fasta_itr(fname)
                if nspace.verbose:
                    sys.stderr.write("Reading depths from %s\n" % (fname))
                rec = itr.next()
                key = rec.header
                depths = numpy.array(rec.sequence.strip().split(), int)
                counter = 0
                for gene in genes:
                    start = min(gene.start(), gene.end()) - 1
                    end = max(gene.start(), gene.end())
                    l = fDict.get(gene.id, [])
                    l.append(depths[start:end])
                    fDict[gene.id] = l
                    counter += 1
                if nspace.verbose:
                    sys.stderr.write("Read %d gene depths\n" % (counter))

    if nspace.verbose:
        sys.stderr.write('-' * 30 + '\n')
        sys.stderr.write("|Reading factor 1 gene depths|\n")
        sys.stderr.write('-' * 30 + '\n')

        print(nspace.factor1files)
    load(nspace.factor1files, f1Dict)
    return f1Dict


def intron_expression(ires, ires_neg):
    annotated = []
    annotated_neg = []

    for entry in ires:
        for i in range(0, len(entry[2])):
            annotated.append(entry[2][i])

    for entry in ires_neg:
        for i in range(0, len(entry[2])):
            annotated_neg.append(entry[2][i])

    annotated_nz = filter(lambda a: a != 0, annotated)
    annotated_neg_nz = filter(lambda a: a != 0, annotated_neg)
    return annotated_nz, annotated_neg_nz


def log_ratios(ires, ires_neg):
    annotated_log = []
    annotated_log_neg = []

    annotated_exons = []
    annotated_exons_neg = []

    for entry in ires:
        for i in range(0, len(entry[2])):
            if entry[1] == 0 or entry[2][i] == 0:
                continue
            else:
                annotated_log.append(math.log(entry[2][i] / entry[1]))
                annotated_exons.append(entry[1])

    for entry in ires_neg:
        for i in range(0, len(entry[2])):
            if entry[1] == 0 or entry[2][i] == 0:
                continue
            else:
                annotated_log_neg.append(math.log(entry[2][i] / entry[1]))
                annotated_exons_neg.append(entry[1])

    annotated_log_nz = filter(lambda a: a != 0, annotated_log)
    annotated_neg_log_nz = filter(lambda a: a != 0, annotated_log_neg)
    return annotated_log_nz, annotated_neg_log_nz


def expression_ratios(ires, ires_neg):
    annotated_exons = []
    annotated_exons_neg = []

    annotated = []
    for entry in ires:
        if len(numpy.nonzero(entry[2])[0]) == 0:
            continue
        else:
            minval = entry[2][numpy.min(numpy.nonzero(entry[2]))]
            if entry[1] != 0:
                annotated.append(minval / entry[1])
                annotated_exons.append(entry[1])

    annotated_neg = []
    for entry in ires_neg:
        if len(numpy.nonzero(entry[2])[0]) == 0:
            continue
        else:
            minval = entry[2][numpy.min(numpy.nonzero(entry[2]))]
            if entry[1] != 0:
                annotated_neg.append(minval / entry[1])
                annotated_exons_neg.append(entry[1])

    annotated_log_nz = filter(lambda a: a != 0, annotated)
    annotated_neg_log_nz = filter(lambda a: a != 0, annotated_neg)
    return annotated_log_nz, annotated_exons, annotated_neg_log_nz, annotated_exons_neg


def check_unspliced(lst, perc):
    length = len(lst)
    count = numpy.count_nonzero(lst)
    perc_calc = (float(count) / length) * 100
    # print perc_calc,count,length
    return False if perc_calc >= perc else True


def load_data_2(args, gene_model):
    temp = args.factor1files[0]
    all_genes = gene_model.getAllGenes()
    return {gene.id: get_depths_from_bam(temp, gene.chromosome, gene.minpos, gene.maxpos)[0] for gene in all_genes}


def main():
    parser = ArgumentParser(description='Calculate gene expression from read depths file')

    parser.add_argument('-o', '--output-file', dest='outfile', action='store',
                        default='expression.txt', help="output file name")
    parser.add_argument('-v', '--verbose', dest='verbose', action='store_true',
                        default=False, help="verbose output")
    parser.add_argument('genemodel', type=str,
                        help="gene model file: NAME.gtf[.gz] | NAME.gff[.gz]")
    parser.add_argument('factor1files', type=file_list,
                        help="colon-separated list of files: PATH-TO-REPLICATE_1[:PATH-TO-REPLICATE_2,...]")

    args = parser.parse_args()
    if not validate_args(args):
        raise Exception("Argument Errors: check usage")

    writeStatus('Loading models')
    gene_model = load_gene_models(args.genemodel, verbose=args.verbose)

    writeStatus('Making reduced models')
    # base_directory = os.path.join(os.path.dirname(args.factor1files[0]), "unsplicing-analysis")
    base_directory = "unsplicing-analysis"
    gene_model_directory = os.path.join(base_directory, 'temp_lists')
    writeStatus('base directory is currently: {}'.format(base_directory))
    if not os.path.exists(os.path.join(gene_model_directory, 'lists')):
        print('making the directory {}'.format(os.path.join(gene_model_directory, 'lists')))
        os.makedirs(os.path.join(gene_model_directory, 'lists'))
        print('made')
    gene_records = make_models(gene_model, gene_model_directory, verbose=args.verbose)

    writeStatus('Loading Depths')
    f1_dict = load_data_2(args, gene_model)
    ires = []
    ires_neg = []
    ires_all = []  # for unspliced vs retained

    count = 0
    count2 = 0
    count_all_two_or_more = 0

    writeStatus('Computing depths')
    with open(os.path.join(base_directory, args.outfile), 'w') as ofile:
        ofile.write('geneID\tf1Exp_gene\tf1Exp_IR\n')

        # these two are to track all intronic and exonic coverages and check the ratio
        all_exonic_coverage = []
        all_intronic_coverage = []

        for gene in gene_records:
            # if gene.gid not in f1Dict or gene.gid not in f2Dict: continue
            if gene.gid not in f1_dict:
                continue
            f1EV = f1_dict[gene.gid]
            if len(f1EV) > 1 and args.verbose:
                print(f1EV, "trueeee")

            F1C = numpy.array([(f1EV[s:(e + 1)]).mean() for s, e in gene.exons])

            all_exonic_coverage.append(F1C.mean())

            # f2EV = f2Dict[gene.gid]
            # F2C = numpy.array([ [(f2EV[i][s:(e+1)]).mean() \
            # for s,e in gene.exons] for i in range(len( f2EV))]).mean(0)
            f1depth = numpy.mean(F1C)  # Used for the exonic coverage, later
            # f2depth = numpy.mean(F2C)
            F1I = numpy.array([(f1EV[s:(e + 1)]).mean() for s, e in gene.introns])

            all_intronic_coverage.append(0 if len(F1I) == 0 else F1I.mean())

            flag = False  # checking flag for all non-zero read positions
            ires1 = []
            ires2 = []
            ires3 = []  # for ires_all

            if len(gene.retained) > 1:  # Count all genes with 2 or more introns
                count_all_two_or_more += 1

            for i, r in enumerate(gene.introns):
                s, e = r
                if check_unspliced(f1EV[s:e], 100):  # check if any position is zero (0 means no unsplicing)
                    flag = True
                    break
                if len(gene.retained) > 1:  # is check for length cuz gene.retained returns [False, True, False,....]
                    if any(item is True for item in gene.retained):  # For IR containing genes only
                        if gene.retained[i]:
                            count += 1
                            ires1.append(numpy.array([(f1EV[s: e]).mean()]).mean(0))
                        ires3.append(numpy.array([(f1EV[s: e]).mean()]).mean(0))
                    else:
                        count2 += 1
                        ires2.append(numpy.array([(f1EV[s: e]).mean()]).mean(0))
                        ires3.append(numpy.array([(f1EV[s: e]).mean()]).mean(0))
            if flag:  # if there are zeros, check another gene
                continue
            if len(ires1) != 0:
                ires_all.append([gene.gid, f1depth, 'retention', ires3])
            elif len(ires2) != 0:
                ires_all.append([gene.gid, f1depth, 'normal', ires3])

            if len(ires1) >= 1:  # >=1 for 1 or more introns in this case I am putting only retained introns in ires1
                ofile.write('%s\t%f\t%f\n' % (gene.gid, f1depth, numpy.mean(ires1)))
                ires.append([gene.gid, f1depth, ires1])
            if len(ires2) > 1:  # >=1 for 1 or more introns
                ires_neg.append([gene.gid, f1depth, ires2])
            else:
                ofile.write('%s\t%f\t-\n' % (gene.gid, f1depth))
            if args.verbose:
                print(count)
                print(count2)

        import matplotlib.pyplot as plt

        annotated_nz, annotated_neg_nz = intron_expression(ires, ires_neg)

        # plt.hist(annotated_nz,bins=100,range=[0,10])
        # plt.show()

        # plt.hist(annotated_neg_nz,bins=100,range=[0,10])
        # plt.show()

        annotated_nz_log, annotated_neg_nz_log = log_ratios(ires, ires_neg)

        # plt.hist(annotated_nz_log,bins=100,range=[-6,6])
        # plt.show()

        # plt.hist(annotated_neg_nz_log,bins=100,range=[-6,6])
        # plt.show()

        annotated = []
        annotated_introns = []
        cnt = 0
        for entry in ires:
            if len(numpy.nonzero(entry[2])[0]) == 0:
                continue
            else:
                cnt += 1
                min_val = entry[2][numpy.min(numpy.nonzero(entry[2]))]
                if entry[1] != 0:
                    annotated.append(min_val / entry[1])
                    annotated_introns.append(min_val)  # just introns

        annotated_neg = []
        annotated_introns_neg = []
        for entry in ires_neg:
            if len(numpy.nonzero(entry[2])[0]) == 0:
                continue
            else:
                min_val = entry[2][numpy.min(numpy.nonzero(entry[2]))]
                if entry[1] != 0:
                    annotated_neg.append(min_val / entry[1])
                    annotated_introns_neg.append(min_val)  # just introns

        # print hists of both annotated and rest: intorns/gene and introns only

        # For Cumulative Distributions-Retained
        # plt.hist(annotated_introns,bins=100,range=[0,20])
        values, base = numpy.histogram(annotated_introns, bins=100, range=[0, 20])
        cumulative = numpy.cumsum(values)
        # plt.plot(base[:-1],cumulative)
        # plt.show()

        # For Cumulative Distributions-Rest
        # plt.hist(annotated_introns_neg,bins=100,range=[0,20])
        values, base = numpy.histogram(annotated_introns_neg, bins=100, range=[0, 20])
        cumulative = numpy.cumsum(values)
        # plt.plot(base[:-1],cumulative)
        # plt.show()

        for entry in ires_all:
            ex_mean = entry[1]
            int_mean = numpy.mean(entry[-1])
            if int_mean != 0 and ex_mean != 0:
                if ex_mean / int_mean <= 2:
                    if numpy.std(entry[-1]) < 4 and numpy.mean(entry[-1]) > 8:
                        print(entry[0], numpy.std(entry[-1]), ex_mean)
                        print(entry[2], entry[-1], int_mean)

        lst_exon = []
        lst_intron = []
        for entry in ires_all:
            lst_exon.append(entry[1])
            lst_intron.append(numpy.mean(entry[-1]))

        # Histogram of the average intronic read depths (where all introns have no zero position covered by a read)
        # plt.hist(lst_intron,bins=100,range=[0,50])
        # plt.title("Histogram: Average Intron Read Depth (100%)")
        # plt.show()

        # Histogram of the ratio of the average intron depth to the average exonic read depth
        ratio = numpy.asarray(lst_intron) / numpy.asarray(lst_exon)
        # plt.title("Histogram: Average Intron to Exon Read Depth Ratio (100%)")
        # plt.hist(ratio,bins=50,range=[0,2])
        # plt.show()

        ##################################TO RUN################################
        # run get_gene_expression_unsplicing_noDepths.py -v -o expression.txt /s/chopin/c/proj/protfun/nobackup/s_bicolor/annotation/genome.gff3 /s/jawar/b/nobackup/altsplice2/Sorghum_New/Sample_R2/TopHat_Aligned/accepted_hits_noMulti.bam
        # for version 1 annotations
        # run get_gene_expression_unsplicing_noDepths.py -v -o expression.txt /s/chopin/c/proj/protfun/nobackup/s_bicolor/annotation/genomeProt.gtf /s/jawar/b/nobackup/altsplice2/Sorghum/Sample_S13_1Control_R1/Star_Aligned_Uniq/depths/
        ########################################################################

        #########To Store Results#########
        # Store 2 files, one with Stats, other with the actual genes#
        directory = os.path.join(base_directory, 'results')
        if not os.path.exists(base_directory):
            os.makedirs(base_directory)
        f_name = args.factor1files[0].split('/')[-3]
        f_name_stats = os.path.join(base_directory, f_name + '-STATS.txt')
        f_name_genes = os.path.join(base_directory, f_name + '-GENES.txt')

        ############Displaying some Final Stats##############
        count_annotated = 0
        count_unannotated = 0

        for entry in ires_all:
            if entry[2] == 'normal':
                count_unannotated += 1
            else:
                count_annotated += 1

        print("\n")
        print('the directory for the unspliced genes info is:', f_name_stats)
        with open(f_name_stats, 'w') as f:
            f.write("Unspliced genes Info\n")
            f.write("total = {}\n".format(len(ires_all)))
            f.write("annotated = {}\n".format(count_annotated))
            f.write("unannotated = {}\n".format(count_unannotated))
            f.write("\n")
            print("the ratio is - it's been getting an error", ratio)
            f.write("Unspliced Genes: Mean intronic vs. Mean Exonic Coverage\n")
            f.write("Mean Ratio = {}".format(ratio.mean()))

            f.write("\n")

            f.write("Mean Intronic vs. Exonic Coverage\n")
            f.write("Intronic vs. Exonic = {}\n".format(
                numpy.mean(all_intronic_coverage) / numpy.mean(all_exonic_coverage)))

            f.write("\n")

            f.write("Intronic Coverage Related\n")
            f.write("Mean All Coverage = {}\n".format(numpy.mean(all_intronic_coverage)))
            f.write("Sum Coverage = {}\n".format(numpy.sum(all_intronic_coverage)))

        print("Stats done!")

        to_save_list = [['gene', 'info']]
        for entry in ires_all:
            to_save_list.append([entry[0], entry[2]])

        numpy.savetxt(f_name_genes, to_save_list, fmt='%s')


if __name__ == "__main__":
    main()
